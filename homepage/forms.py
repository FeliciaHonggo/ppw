from django import forms
from .models import Jadwal

# class jadwalForm(forms.Form):
#     your_name = forms.CharField(label='Your name', max_length=100)




class jadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = [
            'namaKegiatan',
            'hariTanggal',
            'jam',
            'tempat',
            'kategori',
            'deskripsi',
        ]
# yang ini bikin sendiri forms.py nya