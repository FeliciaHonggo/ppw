from django.shortcuts import render, redirect
from .forms import jadwalForm
from .models import Jadwal
from django.utils import timezone
from datetime import datetime, date


# Create your views here.
def website(request):
    return render(request, 'website.html')

def gallery(request):
    return render(request, 'gallery.html')

def jadwal_form(request):
    form = jadwalForm(request.POST or None)
  
    if (form.is_valid() and request.method == 'POST'):
    	
    # 	form.data['namaKegiatan'] = request.POST['namaKegiatan']
    # form.data['hariTanggal'] = request.POST['hariTanggal']
    # form.data['jam'] = request.POST['jam']
    # form.data['tempat'] = request.POST['tempat']
    # form.data['kategori'] = request.POST['kategori']
    # form.data['deskripsi'] = request.POST['deskripsi']
    	jadwal = Jadwal(namaKegiatan=form.data['namaKegiatan'],hariTanggal=form.data['hariTanggal'],
    	jam=form.data['jam'],tempat=form.data['tempat'],
    	kategori=form.data['kategori'],deskripsi=form.data['deskripsi'])
    	jadwal.save()
    	print(form.data['namaKegiatan'])
    form = jadwalForm()
    context_baru = {
        'jadwal_form' : form
    }
    return render(request, "jadwal_form.html", context_baru)

def jadwal_form_output(request):
    form_output = Jadwal.objects.all()
    context = {
        'jadwal_form_outputs' : form_output
    }
    return render(request, "jadwal_form_output.html", context)

def jadwal_delete(request, id):
    jadwal_delete = Jadwal.objects.get(id=id)
    jadwal_delete.delete()
    return redirect('homepage:jadwal_output')
