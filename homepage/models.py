from django.db import models
from django.utils import timezone
from datetime import datetime, date


# Create your models here.
class Jadwal(models.Model):
	namaKegiatan = models.CharField(max_length=30)
	hariTanggal = models.DateField()
	jam = models.TimeField()
	tempat = models.CharField(max_length=120)
	kategori = models.CharField(max_length=120)
	deskripsi = models.TextField(blank=True, null = True)


# 	   CREATE TABLE myapp_person (
#     "id" serial NOT NULL PRIMARY KEY,
#     "first_name" varchar(30) NOT NULL,
#     "last_name" varchar(30) NOT NULL
# );