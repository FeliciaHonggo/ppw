from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.website, name='website'),
    path('gallery/', views.gallery, name='gallery'),
    path('jadwal/', views.jadwal_form, name='jadwal'),
    path('jadwal-output/', views.jadwal_form_output, name='jadwal_output'),
    path('delete/<id>', views.jadwal_delete, name="delete")
    # dilanjutkan ...
]